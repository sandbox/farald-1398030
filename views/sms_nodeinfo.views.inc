<?php

function sms_nodeinfo_views_data() {
    $data = array();
    $data['node'] = array();
    $data['node']['sms_link'] = array(
        'field' => array(
            'title' => t('SMS message link'),
            'help' => t('Provides a SMS message link.'),
            'handler' => 'views_handler_field_node_sms_link',
        ),
    );
    return $data; 
};
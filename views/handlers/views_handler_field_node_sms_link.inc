<?php

/**
 * Field handler to present some custom HTML.
 */
class views_handler_field_node_sms_link extends views_handler_field {

    function construct() {
        parent::construct();
        $this->additional_fields['nid'] = 'nid';
        $this->additional_fields['type'] = 'type';
    }

    function query() {
        $this->ensure_my_table();
        $this->add_additional_fields();
    }

    function pre_render($values) {
        sms_nodeinfo_prepare_link();
    }

    function render($values) {
        $type = $this->get_value($values, 'type');
        $nid = $this->get_value($values, 'nid');
        return sms_nodeinfo_make_link($nid);
    }

}
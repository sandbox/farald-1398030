<?php
/**
 * @file
 * SMS Nodeinfo settings page.
 */

/**
 * Form builder for SMS node info settings
 */
function sms_nodeinfo_settings_form($form, &$form_state) {
  $settings = variable_get('sms_node');
  $entities = entity_get_info();
  $data = array();

  // Collect everything in one setting array
  $form['sms_node'] = array(
      '#tree' => true
  );

  foreach ($entities['node']['bundles'] AS $bundle_id => $bundle) {
    $form['sms_node'][$bundle_id] = array(
        '#title' => t("Nodes of type ") . $bundle['label'],
        '#type' => 'fieldset',
        '#collapsed' => $settings[$bundle_id]['activate'] == 1 ? false : true,
        '#collapsible' => true,
        '#tree' => true,
    );

    $form['sms_node'][$bundle_id]['activate'] = array(
        '#type' => 'checkbox',
        '#title' => 'Enable sms for this node type',
        '#description' => 'Provides SMS sending for the given node type.',
        '#default_value' => $settings[$bundle_id]['activate'],
    );
    $form['sms_node'][$bundle_id]['message'] = array(
        '#type' => 'textarea',
        '#title' => 'Enter the tokenized message text.',
        '#description' => 'Provides SMS sending for the given node type.',
        '#states' => array(
            'invisible' => array(
                'input[name="action"]' => array('checked' => FALSE),
            ),
        ),
        '#default_value' => $settings[$bundle_id]['message'],
    );
  }

  // Available tokens only node
  $token_types = array(
      'node' => 'node'
  );
  // Set up the tokenization
  if (module_exists('token')) {
    $form['store']['token_tree'] = array(
        '#theme' => 'token_tree',
        '#token_types' => $token_types,
        '#states' => array(
            'invisible' => array(
                'input[name="action"]' => array('checked' => FALSE),
            ),
        ),
    );
  }
  return system_settings_form($form);
}
<?php
/**
 * @file
 * SMS Nodeinfo pages.
 * - Page at node/%node/sms
 * - Callback at /sms_nodeinfo/->
 */

/**
 * Form element.
 * Send form at node/¤node/sms
 */
function sms_nodeinfo_form($form, &$form_state, $type, $node) {
  if (!empty($form_state['storage']['node'])) {
    $node = $form_state['storage']['node'];
  } else {
    $form_state['storage']['node'] = $node;
  }



  $form = array();
  $form['message'] = array(
      '#type' => 'textarea',
      '#default_value' => sms_nodeinfo_message($node->nid),
      '#access' => false,
  );
  $form['number'] = array(
      '#type' => 'textfield',
      '#field_prefix' => "",
      '#size' => 20,
      '#maxlength' => 255,
      '#required' => true,
      '#default_value' => t("Your phone number..."),
      '#weight' => 10,
      '#attributes' => array(
          'title' => t('Enter your phone number, and recieve the info.'),
          'OnClick' => 'this.value="";',
      )
  );
  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send'),
      '#weight' => 20,
  );
  //$form['#attached']['css'] = array(drupal_get_path('module', 'sms_nodeinfo') . '/nodeinfo.css',);
  $form['#attributes']['class'][] = 'sms-form';
  return $form;
}

/**
 * Validate function. Checks if sms has been sent to same number & node.
 * Adds an array to $_SESSION to keep track of smses sent.
 * Struct: $_SESSION[sms][nid][phone_number][timestamp]
 * @todo Add setting to turn this on or off.
 *
 * @param type $form
 * @param type $form_state
 */
function sms_nodeinfo_form_validate($form, &$form_state) {

  xdebug_break();
  if (!$form_state['values']['number'] = sms_formatter($form_state['values']['number'])) {
    form_set_error('number', t('Please enter a valid phone number.'));
  }

  // Get values needed & sms array from SESSION.
  $sms = (isset($_SESSION['sms'])) ? $_SESSION['sms'] : array();
  $curr_num = $form_state['values']['number'];
  $curr_node = $form_state['storage']['node'];
  $limit = SMS_NODEINFO_TIME_LIMIT; // The time limit.
  // First: Check if node has been sent to number
  // If yes, check timestamp and invalidate if older than
  if (is_array($sms[$curr_node->type][$curr_num])) {
    $stamp = $sms[$curr_node->type][$curr_num]['stamp'];
    if ((time() < $stamp + $limit)) {
      $time_remain = ($stamp - time()) + $limit;
      // drupal_set_message(t("You already sent this info to this phone number.\n" . "Please try again in $time_remain seconds."), 'error');
      form_set_error("number", t('Message is already sent. Please try again in @time_remain seconds', array('@time_remain' => $time_remain)));
      return;
    }
  }
  $sms[$curr_node][$curr_num]['stamp'] = time();

  // Update session.
  $_SESSION['sms'] = $sms;
}

/**
 * Submit function
 * Sends the actual SMS mesage using smsframework.
 */
function sms_nodeinfo_form_submit($form, &$form_state) {
  if ($form_state['values']['test_me']) {
    drupal_set_message("Message: " . $form_state['values']['message']);
  } elseif (sms_send($form_state['values']['number'], $form_state['values']['message'])) {
    drupal_set_message(t('The message "@message" sent to @number.', array('@message' => $form_state['values']['message'], '@number' => $form_state['values']['number'])));
  } else {
    drupal_set_message("Error sending SMS.", "error");
  }
}

/**
 * CTools Modal callback.
 */
function sms_nodeinfo_ctools_page($js = NULL, $node = NULL) {
  xdebug_break();
  if (!$js) {
    drupal_goto('node/' . $node->nid);
  }

  ctools_include('modal');
  ctools_include('ajax');
  $form_state = array(
      'title' => t('Send node info to mobile'),
      'ajax' => TRUE,
      'storage' => array(
          'node' => $node,
      ),
  );
  $output = ctools_modal_form_wrapper('sms_nodeinfo_form', $form_state);
  if (!empty($form_state['executed'])) {
    xdebug_break();
    ctools_include('ajax');
    ctools_add_js('ajax-responder');
    $commands = array();
    $commands[] = ctools_modal_command_dismiss();
    //$commands[] = ctools_ajax_command_redirect();
    print ajax_render($commands);
    exit;
  }
  print ajax_render($output);
  exit;
}
